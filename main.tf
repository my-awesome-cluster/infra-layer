resource "google_container_cluster" "main" {
    provider    = google-beta
    name        = "my-cluster"

    remove_default_node_pool = true
    initial_node_count       = 1

    monitoring_service       = "none"
    logging_service          = "none"

    addons_config {
        istio_config {
            disabled = var.istio_disabled
        }
        network_policy_config {
            disabled = false
        }
    }

    network_policy {
        enabled = true
        provider = "CALICO"
    }
}

# master nodes
resource "google_container_node_pool" "master_nodes" {
    name        = "master-nodes"
    cluster     = google_container_cluster.main.name

    node_count  = 1

    node_config {
        machine_type = var.master_machine_type

        # taint {
        #     key     = "node-role"
        #     value   = "infra"
        #     effect  = "NO_SCHEDULE"
        # }

        metadata = {
            disable-legacy-endpoints = "true"
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }
}

# worker nodes
resource "google_container_node_pool" "worker_nodes" {
    name        = "worker-nodes"
    cluster     = google_container_cluster.main.name

    node_count  = 1

    node_config {
        machine_type = var.worker_machine_type

        metadata = {
            disable-legacy-endpoints = "true"
        }

        oauth_scopes = [
            "https://www.googleapis.com/auth/logging.write",
            "https://www.googleapis.com/auth/monitoring",
        ]
    }
}

# load balancer IP address
resource google_compute_address "lb_ip_address" {
  name         = "load-balancer-ip-address"
  address_type = "EXTERNAL"
}
