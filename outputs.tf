output "lb_ip_addr" {
  value       = google_compute_address.lb_ip_address.address
  description = "The external IP address for load balancer."
}