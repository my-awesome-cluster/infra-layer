# Quick start guide

Для начала установим переменные окружения для `terraform`

```bash
% export GOOGLE_CREDENTIALS=$(cat auth.json)
% export GOOGLE_PROJECT="<project_id>"
% export GOOGLE_REGION="<region>"
% export GOOGLE_ZONE="<zone>"
```

Получим доступы для `kubctl`

```bash
% gcloud container clusters get-credentials <cluster_name>
```

Создадим GCS бакет для state-файла

```bash
% gsutil mb -b on -l <region> gs://tf-cluster-state
```

# Описание CI пайплайна:

| Этап | Описание |
| --- | --- |
| `validate` | Проверка файлов и конфигурации |
| `plan` | Вывод плана планируемых измениний |
| `apply` | Применение изменений. На этом этапе можно выбрать одно из двух ручных действий - принять изменения (`apply`), или уничтожить кластер (`destroy`) |

> После завершения пайплайна мы получаем `lb_ip_addr`, который будем использовать в проекте [platform-layer](https://gitlab.com/my-awesome-cluster/platform-layer) в качестве IP для `ingress controller'а`