variable "master_machine_type" {
  description = "Master node machine type"
}

variable "worker_machine_type" {
  description = "Worker node machine type"
}

variable "istio_disabled" {
  description = "Istio disabled"
}